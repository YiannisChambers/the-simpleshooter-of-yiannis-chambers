﻿using UnityEngine;
using System.Collections;

//This script manages the player object
public class Player : Spaceship
{
	private float playerShotDelay = 0.25f;
	public GameObject bigBullet;

	void Update ()
	{

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			ShootBigBullet ();
		}

		//Get our raw inputs
		float x = Input.GetAxisRaw ("Horizontal");
		float y = Input.GetAxisRaw ("Vertical");
		//Normalize the inputs
		Vector2 direction = new Vector2 (x, y).normalized;
		//Move the player

		this.transform.rotation = new Quaternion ();//3 (0, 0, 0);

		Move (direction);
	}

	private void ShootBigBullet()
	{
		if (GetComponent<AudioSource>())
			GetComponent<AudioSource>().Play ();
		
		GameObject obj = ObjectPool.current.GetObject(bigBullet);

		//Set its position and rotation
		obj.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z); 
		obj.transform.rotation = this.transform.rotation;
		//Activate it
		obj.SetActive(true);
	}

	protected override void OnEnable ()
	{
		shotDelay = playerShotDelay;
		base.OnEnable ();
	}

	void Move (Vector2 direction)
	{
		//Find the screen limits to the player's movement
		Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		//Get the player's current position
		Vector2 pos = transform.position;
		//Calculate the proposed position

		var newPos = pos + (direction  * speed * Time.deltaTime);

		if (newPos.x > min.x && newPos.x < max.x && newPos.y > min.y && newPos.y < max.y)
		{
			pos += direction * speed * Time.deltaTime;
		}
		//Update the player's position
		transform.position = pos;
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		//Get the layer of the collided object
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		//If the player hit an enemy bullet or ship...
		if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			//...and the object was a bullet...
			if(layerName == "Bullet (Enemy)" )
				//...return the bullet to the pool...
			    ObjectPool.current.PoolObject(c.gameObject) ;
			//...otherwise...
			else
				//...deactivate the enemy ship
				c.gameObject.SetActive(false);

			//Tell the manager that we crashed
			Manager.current.GameOver();
			//Trigger an explosion
			Explode();
			//Deactivate the player
			gameObject.SetActive(false);
		}
	}
}